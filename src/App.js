import { useEffect, useState } from "react";
import { Hero, Navbar, Services, WorkFlow } from "./components";

function App() {
  const [y, setY] = useState(window.scrollY);

  useEffect(() => {
    window.addEventListener("scroll", (e) => setY(e.currentTarget.scrollY));
    
    return () => { 
      window.removeEventListener("scroll", (e) => setY(e.currentTarget.scrollY));
    };
  }, [y]);

  return (
    <>
      <Navbar y={y} />
      <Hero />
      <Services />
      <WorkFlow />
    </>
  );
}

export default App;
