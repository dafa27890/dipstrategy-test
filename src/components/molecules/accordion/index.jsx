import { useContext } from 'react';
import { AccordionContext, Button, Card, Col, Image, Row, useAccordionButton } from 'react-bootstrap';
import Accordion from 'react-bootstrap/Accordion';
import { IconArrowDark, IconArrowLight, IconMinus, IconPlus, ImgAccordion1, ImgAccordion2, ImgAccordion3 } from '../../../assets';
import './style.scss'

const CustomToggle = ({ title,desc,eventKey,callback }) => {
    const { activeEventKey } = useContext(AccordionContext);

    const decoratedOnClick = useAccordionButton(
        eventKey,
        () => callback && callback(eventKey),
      );
    
    const isCurrentEventKey = activeEventKey === eventKey;
    
    return (
        <Row className="">
            <Col md="5" className="d-flex align-items-center">
                <p className="accordion-title">{title}</p>
            </Col>
            <Col md="7" className="d-flex flex-row align-items-center">
                <p className="accordion-desc mb-0">{desc}</p>
                {
                    !isCurrentEventKey ? 
                    <div>
                        <Button className="mx-3 btn-warning">Learn More <Image src={IconArrowDark} /> </Button>
                    </div> : null
                }
                <Image className="cursor-pointer" onClick={decoratedOnClick} src={isCurrentEventKey ? IconMinus : IconPlus} />
            </Col>
        </Row>
    );
  }

const AccordionService = () => {
  return (
    <Accordion defaultActiveKey="0" style={{
        marginTop : '81px',
    }}>
      <Card>
        <Card.Header>
          <CustomToggle 
            title="E-Commerce Development"
            desc="You want to get the best out of your business and turn mere visitors into paying customers? Sell your products on a secure platform and not worry about the technical details? We will help you implement your E-Commerce platform."
            eventKey="0" />
        </Card.Header>
        <Accordion.Collapse eventKey="0">
          <Card.Body>
              <Row>
                  <Col md="6">
                      <Image src={ImgAccordion1} style={{
                          width: '100%'
                      }} />
                  </Col>
                  <Col md="6">
                    <div className="d-flex flex-row justify-content-between">
                        <Image src={ImgAccordion2} style={{
                            width: '45%'
                        }} />
                        <Image src={ImgAccordion3} style={{
                            width: '45%'
                        }} />                        
                    </div>
                    <div className="d-flex flex-row mt-5">
                        <p>It’s time to turn your vision into a high-performing and responsive app. This is how your customers will interact with your business, which is why we pay attention to every detail.</p>
                        <div>
                            <Button className="mx-3 btn-warning">Learn More <Image src={IconArrowDark} /> </Button>
                        </div>
                    </div>
                  </Col>
              </Row>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Card.Header>
          <CustomToggle 
            title="Custom App Development"
            desc="It’s time to turn your vision into a high-performing and responsive app. This is how your customers will interact with your business, which is why we pay attention to every detail."
            eventKey="1" />
        </Card.Header>
        <Accordion.Collapse eventKey="1">
          <Card.Body>
              <Row>
                  <Col md="6">
                      <Image src={ImgAccordion1} style={{
                          width: '100%'
                      }} />
                  </Col>
                  <Col md="6">
                    <div className="d-flex flex-row justify-content-between">
                        <Image src={ImgAccordion2} style={{
                            width: '45%'
                        }} />
                        <Image src={ImgAccordion3} style={{
                            width: '45%'
                        }} />                        
                    </div>
                    <div className="d-flex flex-row mt-5">
                        <p>It’s time to turn your vision into a high-performing and responsive app. This is how your customers will interact with your business, which is why we pay attention to every detail.</p>
                        <div>
                            <Button className="mx-3 btn-warning">Learn More <Image src={IconArrowDark} /> </Button>
                        </div>
                    </div>
                  </Col>
              </Row>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Card.Header>
          <CustomToggle 
            title="Integration With 3rd Party Web Services"
            desc="We use all opportunities to solve business problems. Our intelligent digital strategy and a pragmatic and thoughtful approach to solving business calls deliver a successful framework for both you and your audience."
            eventKey="2" />
        </Card.Header>
        <Accordion.Collapse eventKey="2">
          <Card.Body>
              <Row>
                  <Col md="6">
                      <Image src={ImgAccordion1} style={{
                          width: '100%'
                      }} />
                  </Col>
                  <Col md="6">
                    <div className="d-flex flex-row justify-content-between">
                        <Image src={ImgAccordion2} style={{
                            width: '45%'
                        }} />
                        <Image src={ImgAccordion3} style={{
                            width: '45%'
                        }} />                        
                    </div>
                    <div className="d-flex flex-row mt-5">
                        <p>It’s time to turn your vision into a high-performing and responsive app. This is how your customers will interact with your business, which is why we pay attention to every detail.</p>
                        <div>
                            <Button className="mx-3 btn-warning">Learn More <Image src={IconArrowDark} /> </Button>
                        </div>
                    </div>
                  </Col>
              </Row>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Card.Header>
          <CustomToggle 
            title="Hosting/Maintenance"
            desc="Our hosting services are designed to give you the highest level of security with respect to your website’s performance. We exclusively use safe and modern servers, which minimizes the risk of any single point of failure. This includes a minimized risk of hacks or other malicious activities."
            eventKey="4" />
        </Card.Header>
        <Accordion.Collapse eventKey="4">
          <Card.Body>
              <Row>
                  <Col md="6">
                      <Image src={ImgAccordion1} style={{
                          width: '100%'
                      }} />
                  </Col>
                  <Col md="6">
                    <div className="d-flex flex-row justify-content-between">
                        <Image src={ImgAccordion2} style={{
                            width: '45%'
                        }} />
                        <Image src={ImgAccordion3} style={{
                            width: '45%'
                        }} />                        
                    </div>
                    <div className="d-flex flex-row mt-5">
                        <p>It’s time to turn your vision into a high-performing and responsive app. This is how your customers will interact with your business, which is why we pay attention to every detail.</p>
                        <div>
                            <Button className="mx-3 btn-warning">Learn More <Image src={IconArrowDark} /> </Button>
                        </div>
                    </div>
                  </Col>
              </Row>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Card.Header>
          <CustomToggle 
            title="Design UX/UI"
            desc="Although it might seem merely superficial, the user experience might be the most crucial part of your online presence. When viewers are confronted with long loading times, cramped landing pages or an otherwise infuriating design, they will leave quicker than they came."
            eventKey="5" />
        </Card.Header>
        <Accordion.Collapse eventKey="5">
          <Card.Body>
              <Row>
                  <Col md="6">
                      <Image src={ImgAccordion1} style={{
                          width: '100%'
                      }} />
                  </Col>
                  <Col md="6">
                    <div className="d-flex flex-row justify-content-between">
                        <Image src={ImgAccordion2} style={{
                            width: '45%'
                        }} />
                        <Image src={ImgAccordion3} style={{
                            width: '45%'
                        }} />                        
                    </div>
                    <div className="d-flex flex-row mt-5">
                        <p>It’s time to turn your vision into a high-performing and responsive app. This is how your customers will interact with your business, which is why we pay attention to every detail.</p>
                        <div>
                            <Button className="mx-3 btn-warning">Learn More <Image src={IconArrowDark} /> </Button>
                        </div>
                    </div>
                  </Col>
              </Row>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Card.Header>
          <CustomToggle 
            title="SEO Services"
            desc="A well-thought-out SEO strategy can boost your growth, and in turn sales, without an expensive PPC-campaign. There are multiple ways we can help you with this."
            eventKey="6" />
        </Card.Header>
        <Accordion.Collapse eventKey="6">
          <Card.Body>
              <Row>
                  <Col md="6">
                      <Image src={ImgAccordion1} style={{
                          width: '100%'
                      }} />
                  </Col>
                  <Col md="6">
                    <div className="d-flex flex-row justify-content-between">
                        <Image src={ImgAccordion2} style={{
                            width: '45%'
                        }} />
                        <Image src={ImgAccordion3} style={{
                            width: '45%'
                        }} />                        
                    </div>
                    <div className="d-flex flex-row mt-5">
                        <p>It’s time to turn your vision into a high-performing and responsive app. This is how your customers will interact with your business, which is why we pay attention to every detail.</p>
                        <div>
                            <Button className="mx-3 btn-warning">Learn More <Image src={IconArrowDark} /> </Button>
                        </div>
                    </div>
                  </Col>
              </Row>
          </Card.Body>
        </Accordion.Collapse>
      </Card>      
    </Accordion>
  )
}

export default AccordionService