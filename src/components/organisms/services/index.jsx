import React from 'react'
import { Col, Container, Image, Row } from 'react-bootstrap'
import { IconArrowLight } from '../../../assets'
import AccordionService from '../../molecules/accordion'
import Accordion from '../../molecules/accordion'
import './style.scss'

const Services = () => {
  return (
    <div className="services">
        <Container>
            <p className="services-title">
                Problems Today
            </p>
            <Row style={{
                marginTop : '200px',
            }}>
                <Col md="5">
                    <p className="service-title-left">Our <br /> Services</p>
                </Col>
                <Col md="5">
                    <p className="service-desc">We’ve worked with a wide array of clients across the globe to apply design fundamentals of elegance, simplicity</p>
                </Col>
                <Col md="2" className="d-flex justify-content-end align-items-end icon-arrow">
                    <span>About us</span>
                    <Image src={IconArrowLight} />
                </Col>
            </Row>
            <AccordionService />
        </Container>
    </div>
  )
}

export default Services