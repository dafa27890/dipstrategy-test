import React from 'react'
import { Button, Col, Container, Image, Row } from 'react-bootstrap'
import { IconArrowDark, ImgWorkflow1 } from '../../../assets'
import './style.scss'

const WorkFlow = () => {
  return (
    <Container className="workflow">
        <Row>
            <Col md="6">
                <p className="workflow-titile">Workflow</p>
            </Col>
        </Row>
        <Row>
            <Col md="6">
                <p>It is particularly important for us that you as a customer are actively involved in creative processes and that you know at all times which step we are about to take next. Learn more about our workflow and our communication with you as a customer here.</p>
                <Image src={ImgWorkflow1} style={{
                    marginTop : '90px',
                    width : '100%'
                }} />
            </Col>
            <Col md="6">
                <p className="workflow-sub-title">How we deal with projects from start to finish</p>
                <p>The key to successful collaboration and results that are perfectly tailored to you is clear goal setting and the elimination of misunderstandings.</p>
                <p>Before each project, we therefore obtain detailed information from you about what you expect from our service, what your goals are and what you particularly value. Our experts will also be happy to advise you on this step with their extensive specialist knowledge.</p>
                <p>As soon as your expectations have been clarified, we get down to work. We actively involve you, the customer, in the processes so that you always know where your project currently stands.</p>
                <Button className=" btn-warning">Learn More <Image src={IconArrowDark} /> </Button>
                <div className="d-flex flex-column">
                    <a href="#">How we communicate with the client</a>
                    <a href="#">Stages of the process</a>
                </div>
            </Col>
        </Row>
    </Container>
  )
}

export default WorkFlow