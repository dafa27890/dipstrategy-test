import { Button, Image } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Offcanvas from 'react-bootstrap/Offcanvas';
import { Logo } from '../../../assets';
import './style.scss'

const index = ({y}) => {

console.log(y,'y')

  return (
        <Navbar  bg={y > 300  ? 'black' : 'transparent'} expand='xl' className="mb-3" fixed="top">
          <Container className="align-items-start">
            <Navbar.Brand href="#">
                <Image src={Logo} className={y > 300 ? 'img-scrolled' : ''} />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-xl`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-xl`}
              aria-labelledby={`offcanvasNavbarLabel-expand-xl`}
              placement="end"
            >
              <Offcanvas.Header closeButton>
              <Image src={Logo} className={y > 300 ? 'img-scrolled' : ''} />
              </Offcanvas.Header>
              <Offcanvas.Body>
                <Nav className="justify-content-end flex-grow-1 pe-3">
                  <Nav.Link active>Home</Nav.Link>
                  <Nav.Link>Services •</Nav.Link>
                  <Nav.Link>Work Flow •</Nav.Link>
                  <Nav.Link>Portofolio •</Nav.Link>
                  <Nav.Link>About Us •</Nav.Link>
                  <Nav.Link>Our Value •</Nav.Link>
                  <Button variant="warning">(021) 8583944</Button>
                </Nav>
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
  )
}

export default index