import React from 'react'
import { Col, Container, Image, Row } from 'react-bootstrap'
import { IconArrowDark } from '../../../assets'
import './style.scss'

const Hero = () => {
  return (
    <div className="hero">
        <Container>
            <Row>
                <Col md="9">
                    <p className="hero-title">
                        we assist you in solving tomorrow’s
                    </p>
                </Col>
                <Col md="3" className="d-flex justify-content-end align-items-end icon-arrow">
                    <span>About us</span>
                    <Image src={IconArrowDark} />
                </Col>
            </Row>
        </Container>    
    </div>
  )
}

export default Hero