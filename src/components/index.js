import Navbar from './organisms/navbar';
import Hero from './organisms/hero';
import Services from './organisms/services';
import WorkFlow from './organisms/workflow';

export {
    Navbar,
    Hero,
    Services,
    WorkFlow,
}