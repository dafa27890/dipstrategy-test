import Logo from './img/logo.svg';
import BgHero from './img/bg_hero.png';
import IconArrowDark from './img/icon_arrow_dark.svg';
import IconArrowLight from './img/icon_arrow_light.svg';
import IconPlus from './img/icon_plus.svg';
import IconMinus from './img/icon_minus.svg';
import ImgAccordion1 from './img/img_accordion_1.png';
import ImgAccordion2 from './img/img_accordion_2.png';
import ImgAccordion3 from './img/img_accordion_3.png';
import ImgWorkflow1 from './img/img_workflow_1.png';

export {
    Logo,
    BgHero,
    IconArrowDark,
    IconArrowLight,
    IconPlus,
    IconMinus,
    ImgAccordion1,
    ImgAccordion2,
    ImgAccordion3,
    ImgWorkflow1,
}